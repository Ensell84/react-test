import About from "../components/pages/About";
import React from "react";
import PostIdPage from "../components/pages/PostIdPage";
import Posts from "../components/pages/Posts";
import Login from "../components/pages/Login";
import Error from "../components/pages/Error";

export const privateRoutes = [
    {path: "/about", element: <About/>},
    {path: "/posts", element: <Posts/>},
    {path: "/posts/:id", element: <PostIdPage/>},
    {path: "/error", element: <Error/>}
]

export const publicRoutes = [
    {path: "/login", element: <Login/>},
]