import React from 'react';
import MyButton from "./UI/button/MyButton";
import {useNavigate} from "react-router-dom";

const Postitem = ({post: {title, body, id}, post, remove}) => {
    const router = useNavigate();
    return (
        <div className="post">
            <div className="post__content">
                <strong>{id}. {title}</strong>
                <div>
                    {body}
                </div>
            </div>
            <div className="post__btns">
                <MyButton style = {{marginRight: "3px"}} onClick = {() => router("/posts/" + id)}>Open</MyButton>
                <MyButton onClick = {() => remove(post)}>Delete</MyButton>
            </div>
        </div>
    );
};

export default Postitem;