import React from 'react';
import classes from './MySelect.module.css'

const MySelect = ({defaultValue, options, value, onChange}) => {
    return (
        <select
            className={classes.mySlc}
            value={value}
            onChange={e => onChange(e.target.value)}
        >
            <option disabled>{defaultValue}</option>
            {options.map((option) =>
                <option value={option.value} key={option.value}>
                    {option.name}
                </option>
            )}
        </select>
    );
};

export default MySelect;