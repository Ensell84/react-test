import React, {useEffect, useState} from "react";
import {useFetching} from "../../hooks/useFetching";
import Pagination from "../UI/pagination/Pagination";
import PostForm from "../PostForm";
import {getPagesCount} from "../../utils/getPagesCount";
import PostService from "../../API/PostService";
import MyButton from "../UI/button/MyButton";
import MyModal from "../UI/MyModal/MyModal";
import PostFilter from "../PostFilter";
import Loader from "../UI/loader/Loader";
import PostList from "../PostList";
import {usePosts} from "../../hooks/usePosts";
import "../../styles/App.css"


function Posts() {
    const [posts, setPosts] = useState([]);
    const [filter, setFilter] = useState({ sort: "", query: "" });
    const [modal, setModal] = useState(false);

    const [totalPages, setTotalPages] = useState(0);
    const [limit, setLimit] = useState(10);
    const [page, setPage] = useState(1);

    const sortedAndSearchedPosts = usePosts(posts, filter.sort, filter.query);
    const [fetchPosts, postError, isPostsLoading] = useFetching( async () => {
        const response = await PostService.getAll(limit, page);
        setPosts(response.data);
        const totalCount = response.headers['x-total-count'];
        setTotalPages(getPagesCount(totalCount, limit));
    })

    useEffect(fetchPosts,[page]);

    const createPost = (newPost) => {
        setPosts([...posts, newPost]);
        setModal(false);
    };

    const removePost = (remPost) => {
        setPosts(posts.filter((p) => p.id !== remPost.id));
    };

    return (
        <div className="App">
            <MyButton onClick={() => setModal(true)}>Создать пост</MyButton>
            <MyModal visible={modal} setVisible={setModal}>
                <PostForm create={createPost} />
            </MyModal>

            <hr style={{ margin: "15px 0" }} />
            <PostFilter filter={filter} setFilter={setFilter} />
            {postError && <h1>Произошла ошибка: {postError}</h1>}
            {isPostsLoading
                ? <div style={{display: "flex", justifyContent: "center", marginTop: "200px" }}><Loader/></div>
                : <PostList
                    remove={removePost}
                    posts={sortedAndSearchedPosts}
                    title={"JS posts:"}
                />
            }
            <Pagination page={page} setPage={setPage} totalPages={totalPages}/>
        </div>
    );
}

export default Posts;
