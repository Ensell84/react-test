import React, {useEffect, useState} from 'react';
import {useParams} from "react-router-dom";
import {useFetching} from "../../hooks/useFetching";
import PostService from "../../API/PostService";
import Loader from "../UI/loader/Loader";
import {findAllByDisplayValue} from "@testing-library/react";

const PostIdPage = () => {
    const parameters = useParams();
    const [post, setPost] = useState({});
    const [comments, setComments] = useState([]);

    const [fetchPostById, isPostLoading, postError] = useFetching(async () => {
        const response = await PostService.getById(parameters.id);
        setPost(response.data);
    })

    const [fetchComments, isCommentsLoading, commentsError] = useFetching(async () => {
        const response = await PostService.getCommentsById(parameters.id);
        setComments(response.data);
    })

    useEffect(fetchPostById, [])
    useEffect(fetchComments, [])

    return (
        <div className="post_page">
            <h1> Пост номер: {parameters.id}</h1>
            {isPostLoading
                ? <div style={{display: "flex", justifyContent: "center", marginTop: "200px" }}><Loader/></div>
                : <div className="post_section"><h3>{post.title}</h3> <br/> {post.body}</div>
            }
            <h3>Комментарии:</h3>
            <div className="comments">
                {comments.map(comm =>
                    <div className="comment" key={comm.id}>
                        <h5>{comm.name}</h5>
                        <div>{comm.body}</div>
                    </div>
                )}
            </div>
        </div>
    );
};

export default PostIdPage;