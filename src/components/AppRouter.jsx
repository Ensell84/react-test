import React, {useContext} from 'react';
import {Route, Routes} from "react-router-dom";
import Posts from "./pages/Posts";
import {privateRoutes, publicRoutes} from "../router/router";
import {AuthContext} from "../context/context";
import Login from "./pages/Login";

const AppRouter = () => {
    const {isAuth, setIsAuth} = useContext(AuthContext);
    return (
        isAuth
            ?
            <Routes>
                {privateRoutes.map(route =>
                    <Route key = {route.path} path={route.path} element = {route.element}/>
                )}
                <Route path="*" element={<Posts/>}/>
            </Routes>
            :
            <Routes>
                {publicRoutes.map(route =>
                    <Route key = {route.path} path={route.path} element = {route.element}/>
                )}
                <Route path="*" element={<Login/>}/>
            </Routes>
    );
};

export default AppRouter;